﻿using Attempt.Write.Domain.Models.Case;
using Attempt.Write.Domain.Models.Case.Entities;
using Attempt.Write.Domain.Models.Case.Events;
using Attempt.Write.Domain.Models.Case.ValueObjects;
using Attempt.Write.Domain.Models.Judge;
using EventFlow.Aggregates;
using EventFlow.MongoDB.ReadStores;
using EventFlow.MongoDB.ReadStores.Attributes;
using EventFlow.ReadStores;

namespace Attempt.Read.Cases
{
    [MongoDbCollectionName("cases")]
    public class CaseReadModelForMongo : IMongoDbReadModel,
        IAmReadModelFor<CaseAggregate, CaseId, CaseCreatedEvent>,
        IAmReadModelFor<CaseAggregate, CaseId, JudgeAssignedEvent>,
        IAmReadModelFor<CaseAggregate, CaseId, PartyAssignedEvent>
    {
        public string _id { get; set; }
        public CaseId Id { get; set; }
        public long? _version { get; set; }
        public DocketNumber DocketNumber { get; private set; }
        public Judge Judge { get; private set; }
        public Party Party { get; private set; }

        public void Apply(IReadModelContext context, IDomainEvent<CaseAggregate, CaseId, CaseCreatedEvent> domainEvent)
        {
            _id = domainEvent.AggregateIdentity.Value;
            Id = domainEvent.AggregateIdentity;
            DocketNumber = domainEvent.AggregateEvent.DocketNumber;
        }

        public void Apply(IReadModelContext context,
            IDomainEvent<CaseAggregate, CaseId, JudgeAssignedEvent> domainEvent)
        {
            Judge = domainEvent.AggregateEvent.Judge;

        }

        public void Apply(IReadModelContext context, IDomainEvent<CaseAggregate, CaseId, PartyAssignedEvent> domainEvent)
        {
            Party = domainEvent.AggregateEvent.Party;
        }

        public Case ToCase()
        {
            return new Case(Id, DocketNumber, Judge);
        }
    }
}
