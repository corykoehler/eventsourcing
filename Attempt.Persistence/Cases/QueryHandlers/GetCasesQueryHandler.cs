﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Attempt.Write.Domain.Models.Case;
using Attempt.Write.Domain.Models.Case.Queries;
using EventFlow.Queries;
using EventFlow.ReadStores.InMemory;

namespace Attempt.Read.Cases
{
    public class GetCasesQueryHandler : IQueryHandler<GetCasesQuery, IReadOnlyCollection<Case>>
    {
        private readonly IInMemoryReadStore<CaseReadModel> _readStore;
        public GetCasesQueryHandler(IInMemoryReadStore<CaseReadModel> readstore)
        {
            _readStore = readstore;
        }

        public async Task<IReadOnlyCollection<Case>> ExecuteQueryAsync(GetCasesQuery query, CancellationToken cancellationToken)
        {
            var caseIds = new HashSet<CaseId>(query.CaseIds);
            var caseReadModels = await _readStore.FindAsync(
                    rm => caseIds.Contains(rm.Id), cancellationToken)
                .ConfigureAwait(false);

            return caseReadModels.Select(rm => rm.ToCase()).ToList();
        }
    }
}
