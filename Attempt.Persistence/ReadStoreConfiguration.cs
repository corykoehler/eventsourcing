﻿using System.Reflection;
using Attempt.Read.Cases;
using EventFlow;
using EventFlow.Configuration;
using EventFlow.Extensions;
using EventFlow.MongoDB.Extensions;

namespace Attempt.Read
{
    public static class ReadStoreConfiguration
    {
        public static Assembly Assembly { get; } = typeof(ReadStoreConfiguration).Assembly;

        public static IEventFlowOptions ConfigureEventFlowOptions(this IEventFlowOptions eventFlowOptions)
        {
            return eventFlowOptions
                .AddQueryHandlers(Assembly);

        }

        public static IRootResolver CreateRootResolver(this IEventFlowOptions eventFlowOptions)
        {
            var resolver = eventFlowOptions
                .ConfigureMongoDb("someconnectionstring", "ReadStore")
                .UseMongoDbReadModel<CaseReadModelForMongo>()
                .CreateResolver();

            return resolver;
        }
    }
}
