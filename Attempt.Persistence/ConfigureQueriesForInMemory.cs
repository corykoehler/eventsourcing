﻿using System.Reflection;
using Attempt.Read.Cases;
using EventFlow;
using EventFlow.Extensions;

namespace Attempt.Read
{
    public static class ConfigureQueriesForInMemory
    {
        public static Assembly Assembly { get; } = typeof(ConfigureQueriesForInMemory).Assembly;

        public static IEventFlowOptions ConfigureQueriesInMemory(this IEventFlowOptions eventFlowOptions)
        {
            return eventFlowOptions
                .AddQueryHandlers(Assembly)
                .UseInMemoryReadStoreFor<CaseReadModel>();
        }
    }
}
