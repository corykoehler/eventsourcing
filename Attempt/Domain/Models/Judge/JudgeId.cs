﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Attempt.Write.Domain.Models.Judge
{
    [JsonConverter(typeof(SingleValueObjectConverter))]
    public class JudgeId : Identity<JudgeId>
    {
        public JudgeId(string value) : base(value)
        {
            
        }
    }
}
