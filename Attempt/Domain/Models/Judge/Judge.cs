﻿using EventFlow.Entities;

namespace Attempt.Write.Domain.Models.Judge
{
    public class Judge : Entity<JudgeId>
    {
        public Judge(JudgeId id, string name, string barNumber) : base(id)
        {
            Name = name;
            BarNumber = BarNumber;
        }

        public string Name { get; }
        public string BarNumber { get; }
    }
}
