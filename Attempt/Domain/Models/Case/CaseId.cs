﻿using EventFlow.Core;

namespace Attempt.Write.Domain.Models.Case
{
    public class CaseId : Identity<CaseId>
    {
        public CaseId(string value) : base(value)
        {   
        }
    }
}
