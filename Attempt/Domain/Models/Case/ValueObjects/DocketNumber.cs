﻿using EventFlow.ValueObjects;

namespace Attempt.Write.Domain.Models.Case.ValueObjects
{
    public class DocketNumber : ValueObject
    {
        public DocketNumber(string year, string caseNumber, string caseType)
        {
            Year = year;
            CaseNumber = caseNumber;
            CaseType = caseType;
        }

        public string Year { get; }
        public string CaseNumber { get; }
        public string CaseType { get; }
    }
}
