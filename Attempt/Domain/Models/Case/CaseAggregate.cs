﻿using Attempt.Write.Domain.Models.Case.Entities;
using Attempt.Write.Domain.Models.Case.Events;
using Attempt.Write.Domain.Models.Case.ValueObjects;
using EventFlow.Aggregates;

namespace Attempt.Write.Domain.Models.Case
{
    public class CaseAggregate : AggregateRoot<CaseAggregate, CaseId>
    {
        private readonly CaseState _state = new CaseState();

        public CaseAggregate(CaseId id): base(id)
        {
            Register(_state);
        }

        public DocketNumber DocketNumber => _state.DocketNumber;
        public Judge.Judge Judge => _state.Judge;
        public Party Party => _state.Party;

        public void CreateCase(DocketNumber docketNumber)
        {
            Emit(new CaseCreatedEvent(docketNumber));
        }

        public void AssignJudge(Judge.Judge judge)
        {
            Emit(new JudgeAssignedEvent(judge));
        }

        public void AssignParty(Party party)
        {
            Emit(new PartyAssignedEvent(party));
        }
    }
}
