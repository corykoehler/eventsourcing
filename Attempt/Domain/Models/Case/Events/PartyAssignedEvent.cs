﻿using EventFlow.Aggregates;
using EventFlow.EventStores;
using Attempt.Write.Domain.Models.Case.Entities;

namespace Attempt.Write.Domain.Models.Case.Events
{
    [EventVersion("Party Assigned", 1)]
    public class PartyAssignedEvent : AggregateEvent<CaseAggregate, CaseId>
    {
        public PartyAssignedEvent(Party party)
        {
            Party = party;
        }

        public Entities.Party Party { get; }
    }
}
