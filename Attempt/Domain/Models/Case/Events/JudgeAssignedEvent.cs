﻿using Attempt.Write.Domain.Models.Case.Entities;
using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace Attempt.Write.Domain.Models.Case.Events
{
    [EventVersion("Judge Assigned", 1)]
    public class JudgeAssignedEvent : AggregateEvent<CaseAggregate, CaseId>
    {
        public JudgeAssignedEvent(Judge.Judge judge)
        {
            Judge = judge;
        }

        public Judge.Judge Judge { get; }
    }
}
