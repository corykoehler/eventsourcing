﻿using Attempt.Write.Domain.Models.Case.ValueObjects;
using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace Attempt.Write.Domain.Models.Case.Events
{
    [EventVersion("CaseCreated", 1)]
    public class CaseCreatedEvent : AggregateEvent<CaseAggregate, CaseId>
    {
        public CaseCreatedEvent(DocketNumber docketNumber)
        {
            DocketNumber = docketNumber;
        }

        public DocketNumber DocketNumber { get; }
    }
}
