﻿using Attempt.Write.Domain.Models.Case.ValueObjects;
using EventFlow.Entities;

namespace Attempt.Write.Domain.Models.Case
{
    public class Case : Entity<CaseId>
    {
        public Case(CaseId id, DocketNumber docketNumber, Judge.Judge judge) : base(id)
        {
            DocketNumber = docketNumber;
            Judge = judge;
        }

        public DocketNumber DocketNumber { get; }
        public Judge.Judge Judge { get; }
    }
}
