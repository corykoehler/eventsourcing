﻿using Attempt.Write.Domain.Models.Case.Entities;
using Attempt.Write.Domain.Models.Case.Events;
using Attempt.Write.Domain.Models.Case.ValueObjects;
using EventFlow.Aggregates;

namespace Attempt.Write.Domain.Models.Case
{
    public class CaseState : AggregateState<CaseAggregate, CaseId, CaseState>,
        IApply<CaseCreatedEvent>,
        IApply<JudgeAssignedEvent>,
        IApply<PartyAssignedEvent>
    {
       public DocketNumber DocketNumber { get; private set; }
       public Judge.Judge Judge { get; private set; }
        public Party Party { get; private set; }

        public void Apply(CaseCreatedEvent aggregateEvent)
        {
            DocketNumber = aggregateEvent.DocketNumber;
        }

        public void Apply(JudgeAssignedEvent aggregateEvent)
        {
            Judge = aggregateEvent.Judge;
        }

        public void Apply(PartyAssignedEvent aggregateEvent)
        {
            Party = aggregateEvent.Party;
        }
    }
}