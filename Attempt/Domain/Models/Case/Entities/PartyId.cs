﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Attempt.Write.Domain.Models.Case.Entities
{
    [JsonConverter(typeof(SingleValueObjectConverter))]
    public class PartyId : Identity<PartyId>
    {
        public PartyId(string value) : base(value)
        {
            
        }
    }
}
