﻿using EventFlow.Entities;

namespace Attempt.Write.Domain.Models.Case.Entities
{
    public class Party : Entity<PartyId>
    {
        public Party(PartyId id, string partyType) : base(id)
        {
            PartyType = partyType;
        }

        public string PartyType { get; }
    }
}
