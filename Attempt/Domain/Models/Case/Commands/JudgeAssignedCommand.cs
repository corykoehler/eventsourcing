﻿using EventFlow.Commands;

namespace Attempt.Write.Domain.Models.Case.Commands
{
    public class JudgeAssignedCommand : Command<CaseAggregate, CaseId>
    {
        public JudgeAssignedCommand(CaseId aggregateId, Judge.Judge judge) : base(aggregateId)
        {
            Judge = judge;
        }
        public Judge.Judge Judge { get; }
    }
}
