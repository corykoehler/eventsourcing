﻿using Attempt.Write.Domain.Models.Case.Entities;
using EventFlow.Commands;

namespace Attempt.Write.Domain.Models.Case.Commands
{
    public class PartyAssignedCommand : Command<CaseAggregate, CaseId>
    {
        public PartyAssignedCommand(CaseId aggregateId, Party party) : base(aggregateId)
        {
            Party = party;
        }

        public Party Party { get; }
    }
}