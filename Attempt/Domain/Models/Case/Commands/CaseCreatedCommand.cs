﻿using Attempt.Write.Domain.Models.Case.ValueObjects;
using EventFlow.Commands;

namespace Attempt.Write.Domain.Models.Case.Commands
{
    public class CaseCreatedCommand : Command<CaseAggregate, CaseId>
    {
        public CaseCreatedCommand(CaseId aggregateId, DocketNumber docketNumber) : base(aggregateId)
        {
            DocketNumber = docketNumber;
        }

        public DocketNumber DocketNumber { get; }
    }
}
