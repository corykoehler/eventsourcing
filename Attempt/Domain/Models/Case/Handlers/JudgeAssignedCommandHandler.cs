﻿using System.Threading;
using System.Threading.Tasks;
using Attempt.Write.Domain.Models.Case.Commands;
using EventFlow.Commands;

namespace Attempt.Write.Domain.Models.Case.Handlers
{
    public class JudgeAssignedCommandHandler : CommandHandler<CaseAggregate, CaseId, JudgeAssignedCommand>
    {
        public override Task ExecuteAsync(CaseAggregate aggregate, JudgeAssignedCommand command, CancellationToken cancellationToken)
        {
            aggregate.AssignJudge(command.Judge);
            return Task.FromResult(0);
        }
    }
}
