﻿using System.Threading;
using System.Threading.Tasks;
using Attempt.Write.Domain.Models.Case.Commands;
using EventFlow.Commands;

namespace Attempt.Write.Domain.Models.Case.Handlers
{
    public class PartyAssignedCommandHandler : CommandHandler<CaseAggregate, CaseId, PartyAssignedCommand>
    {
        public override Task ExecuteAsync(CaseAggregate aggregate, PartyAssignedCommand command, CancellationToken cancellationToken)
        {
            aggregate.AssignParty(command.Party);
            return Task.FromResult(0);
        }
    }
}
