﻿using System.Threading;
using System.Threading.Tasks;
using Attempt.Write.Domain.Models.Case.Commands;
using EventFlow.Commands;

namespace Attempt.Write.Domain.Models.Case.Handlers
{
    public class CaseCreatedCommandHandler : CommandHandler<CaseAggregate, CaseId, CaseCreatedCommand>
    {
        public override Task ExecuteAsync(CaseAggregate aggregate, CaseCreatedCommand command, CancellationToken cancellationToken)
        {
            aggregate.CreateCase(command.DocketNumber);
            var task = Task.FromResult(0);
            return task;
        }
    }
}
