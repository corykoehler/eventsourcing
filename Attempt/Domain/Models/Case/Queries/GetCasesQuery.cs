﻿using System.Collections.Generic;
using System.Linq;
using EventFlow.Queries;

namespace Attempt.Write.Domain.Models.Case.Queries
{
    public class GetCasesQuery : IQuery<IReadOnlyCollection<Case>>
    {
        public GetCasesQuery(params CaseId[] caseIds) : this((IEnumerable<CaseId>) caseIds)
        {
        }

        public GetCasesQuery(IEnumerable<CaseId> caseIds)
        {
            CaseIds = caseIds.ToList();
        }

        public IReadOnlyCollection<CaseId> CaseIds { get; }
    }
}
