﻿using System.Reflection;
using Attempt.Write.Domain.Models.Case.Commands;
using Attempt.Write.Domain.Models.Case.Events;
using EventFlow;
using EventFlow.Extensions;
using EventFlow.MongoDB.Extensions;

namespace Attempt.Write
{
    public static class EventFlowConfiguration
    {
        public static Assembly Assembly { get; } = typeof(EventFlowConfiguration).Assembly;

        public static IEventFlowOptions ConfigureCaseDomain(this IEventFlowOptions eventFlowOptions)
        {
            return eventFlowOptions
                .AddDefaults(Assembly)
                .AddEvents(typeof(CaseCreatedEvent), typeof(JudgeAssignedEvent), typeof(PartyAssignedEvent))
                .AddCommands(typeof(CaseCreatedCommand), typeof(JudgeAssignedCommand), typeof(PartyAssignedCommand))
                //.ConfigureMongoDb("someurl", "mongodbname")
                //.UseMongoDbEventStore()
                //.UseMongoDbSnapshotStore()
                .RegisterServices(sr =>
                {
                    sr.Register<ICaseService, CaseService>();
                });
        }
    }
}
