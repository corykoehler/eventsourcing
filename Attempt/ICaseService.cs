﻿using System.Threading;
using System.Threading.Tasks;
using Attempt.Write.Domain.Models.Case;

namespace Attempt.Write
{
    public interface ICaseService
    {
        Task<CaseId> CreateCaseAsync(string year, string caseNumber, string caseType,
            CancellationToken cancellationToken);
    }
}