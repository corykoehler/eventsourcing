﻿using EventFlow;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Attempt.Write.Domain.Models.Case;
using Attempt.Write.Domain.Models.Case.Commands;
using Attempt.Write.Domain.Models.Case.ValueObjects;

namespace Attempt.Write
{
    public class CaseService : ICaseService
    {
        private readonly ICommandBus _commandBus;

        public CaseService(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public async Task<CaseId> CreateCaseAsync(string year, string caseNumber, string caseType, CancellationToken cancellationToken)
        {
            var caseId = CaseId.New;
            await _commandBus.PublishAsync(new CaseCreatedCommand(caseId, new DocketNumber(year, caseNumber, caseType)), cancellationToken).ConfigureAwait(false);

            return caseId;
        }
    }
}
