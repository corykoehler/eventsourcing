﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Attempt.Read;
using Attempt.Write;
using Attempt.Write.Domain.Models.Case;
using Attempt.Write.Domain.Models.Case.Commands;
using Attempt.Write.Domain.Models.Case.Handlers;
using Attempt.Write.Domain.Models.Case.ValueObjects;
using AutoFixture;
using EventFlow;
using EventFlow.Aggregates;
using EventFlow.Configuration;
using NSubstitute;
using Xunit;

namespace Attempt.Tests
{
    public class CaseCreatedCommandHandlerTests
    {
        private readonly Fixture _fixture = new Fixture();

        private readonly IRootResolver _resolver;
        private readonly IAggregateStore _aggregateStore;
        private readonly ICommandBus _commandBus;

        public CaseCreatedCommandHandlerTests()
        {
            _resolver = EventFlowOptions.New
                .ConfigureCaseDomain()
                .ConfigureQueriesInMemory()
                .CreateResolver(true);
            _aggregateStore = _resolver.Resolve<IAggregateStore>();
            _commandBus = _resolver.Resolve<ICommandBus>();
        }

        [Fact]
        public void CreateCommandCallsHandler()
        {
            var caseId = CaseId.New;
            var docketNumber = _fixture.Create<DocketNumber>();
            var caseCreatedCommand = new CaseCreatedCommand(caseId, docketNumber);

            var handler = Substitute.For<CaseCreatedCommandHandler>();
            handler.ExecuteAsync(Arg.Any<CaseAggregate>(), Arg.Any<CaseCreatedCommand>(), Arg.Any<CancellationToken>())
                .Returns(Task.CompletedTask);

            _commandBus.PublishAsync(caseCreatedCommand, CancellationToken.None);

            handler.Received(1).ExecuteAsync(Arg.Any<CaseAggregate>(), Arg.Any<CaseCreatedCommand>(), Arg.Any<CancellationToken>());
        }
    }
}
