﻿using System.Threading;
using System.Threading.Tasks;
using Attempt.Read;
using Attempt.Write;
using Attempt.Write.Domain.Models.Case;
using Attempt.Write.Domain.Models.Case.Commands;
using Attempt.Write.Domain.Models.Case.ValueObjects;
using AutoFixture;
using EventFlow;
using EventFlow.Aggregates;
using EventFlow.Configuration;
using FluentAssertions;
using Xunit;

namespace Attempt.Tests
{
    public class CaseServiceTests
    {
        private readonly Fixture _fixture = new Fixture();

        private readonly IRootResolver _resolver;
        private readonly IAggregateStore _aggregateStore;
        private readonly ICommandBus _commandBus;

        public CaseServiceTests()
        {
            _resolver = EventFlowOptions.New
                .ConfigureCaseDomain()
                .ConfigureQueriesInMemory()
                .CreateResolver(true);
            _aggregateStore = _resolver.Resolve<IAggregateStore>();
            _commandBus = _resolver.Resolve<ICommandBus>();
        }

        [Fact]
        public async Task CaseIsCreatedAndEventIsStored()
        {
            var docketNumber = _fixture.Create<DocketNumber>();

            var service = _resolver.Resolve<ICaseService>();
            var caseid = await service.CreateCaseAsync(docketNumber.Year, docketNumber.CaseNumber, docketNumber.CaseType,
                CancellationToken.None).ConfigureAwait(false);

           var @case = await _aggregateStore.LoadAsync<CaseAggregate, CaseId>(caseid, CancellationToken.None);

            @case.Id.Should().Be(caseid);
        }

    }
}
